<?php 
    function selectedSeMesmoTipo(Produto $produto, $tipo)
    {
        echo $produto->getTipoProduto() == $tipo? 'selected="selected"' : '';
    }
?>
<tr>
    <td>Nome :</td> 
    <td><input class="form-control" type="text" name="nome" value="<?php echo $produto->getNome(); ?>"/></td>
</tr>
<tr>
    <td>Preço:</td>
    <td><input class="form-control" type="number" name="preco" value="<?php echo $produto->getPreco(); ?>"/></td>
</tr>
<tr>
    <td>Descrição:</td>
    <td><textarea class="form-control" type="text" name="descricao"><?php echo $produto->getDescricao(); ?></textarea></td>
</tr>
<tr>
    <td><input type="checkbox" name="usado" <?php echo $usadoChecked; ?>/>Usado</td>
</tr>
<tr>
    <td>Categoria:</td>
    <td>
        <select name="categoria_id" class="form-control">
            <?php
            foreach ($categorias as $categoria):
                $selecao = ($produto->getCategoriaId() === $categoria->getId()) ? 'selected="selected"' : "";
            ?>
                <option value="<?php echo $categoria->getId(); ?>" 
                    <?php echo $selecao; ?>>
                    <?php echo $categoria->getNome(); ?><br>
                </option>
            <?php endforeach; ?>
        </select>
    </td>
</tr>
<tr>
    <td>Tipo de Produto:</td>
    <td>
        <select name="tipo_produto" class="form-control">
            <option <?php selectedSeMesmoTipo($produto, "LivroFisico"); ?> value="LivroFisico">Livro Físico</option>
            <option <?php selectedSeMesmoTipo($produto, "Ebook"); ?> value="Ebook">Ebook</option>
        </select>
    </td>
</tr>
<tr>
    <td>ISBN (somente se for livro)</td>
    <td><input type="text" name="isbn" class="form-control" value="<?php echo $produto->get("Isbn") ;?>" /></td>
</tr>