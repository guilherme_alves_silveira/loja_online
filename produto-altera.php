<?php
require_once("cabecalho.php");
verificaUsuario();
$produto = ProdutoFactory::criaPor($_POST["tipo_produto"]);
$produto->atualizaBaseadoEm($_POST);
$produto->setId($_POST["id"]);
$produtoDAO = new ProdutoDAO($conexao);

$isAltSucesso = $produtoDAO->altera($produto);
if ($isAltSucesso)
{
    echo "<p class='text-success'>O produto {$produto->getNome()} de preço {$produto->getPreco()} foi alterado com sucesso!</p>";
}
else
{
    echo "<p class='text-danger'>O produto {$produto->getNome()} não foi alterado.</p>";
}

require_once("rodape.php");