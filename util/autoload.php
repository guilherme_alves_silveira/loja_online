<?php

function carregaClasse($nomeDaClasse)
{
    $locais = array("class/model/", "class/dao/", "class/util/");
    foreach ($locais as $local)
    {
        $classe = $local . $nomeDaClasse . ".php";
        if (file_exists($classe))
        {
            require_once($classe);
        }
    }
}

spl_autoload_register("carregaClasse");