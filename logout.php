<?php
require_once("autenticacao.php");
if (usuarioEstaLogado())
{
    deslogaUsuario();
    colocaMsgSuccess("Usuário deslogado com sucesso.");
    header("Location:index.php");
}
die();