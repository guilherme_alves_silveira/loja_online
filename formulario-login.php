<h2>Login</h2>
<form action="login.php" method="post">
    <table class="table">
        <tr>
            <td>E-mail:</td>
            <td><input class="form-control" type="email" name="email" maxlength="50"/></td>
        </tr>
        <tr>
            <td>Senha:</td>
            <td><input class="form-control" type="password" name="senha" maxlength="16"/></td>
        </tr>
        <tr>
            <td><button type="submit" class="btn btn-primary btn-lg btn-block">Logar</button></td>
        </tr>
    </table>
</form>