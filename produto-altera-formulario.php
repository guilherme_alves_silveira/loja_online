<?php
require_once("cabecalho.php");
verificaUsuario();

$produtoDAO = new ProdutoDAO($conexao);
$categoriaDAO = new CategoriaDAO($conexao);
$id = $_GET["id"];
$produto = $produtoDAO->buscaPorId($id);
$categorias = $categoriaDAO->lista();
$usadoChecked = $produto->getUsado()? 'checked="checked"' : "";
?>
<h1>Altere os dados do Produto</h1>
<form action="produto-altera.php" method="post">
    <table class="table">
        <input type="hidden" name="id" value="<?php echo $produto->getId(); ?>">
        <?php include("produto-formulario-base.php"); ?>
        <tr>
            <td><input class="btn btn-primary btn-lg" type="submit" value="Alterar Produto"/></td>
        </tr>
    </table>
</form>
<?php require_once("rodape.php");