<?php

session_start();
require_once("PHPMailer/class.phpmailer.php");
require_once("PHPMailer/class.pop3.php");
require_once("PHPMailer/class.smtp.php");
require_once("PHPMailer/PHPMailerAutoload.php");

$nome = $_POST["nome"];
$email = $_POST["email"];
$mensagem = $_POST["mensagem"];

$mail = new PHPMailer();

$mail->isSMTP();
$mail->Host = "smtp.gmail.com";
$mail->Port = 587;
$mail->SMTPSecure = "tls";
$mail->SMTPAuth = true;
$mail->Username = "teste@hotmail.com";
$mail->Password = "12345";

$mail->setFrom("teste@hotmail.com", "Empresa de Controle de Produtos");
$mail->addAddress("teste@hotmail.com");

$mail->Subject = "E-mail de contato da loja";
$mail->msgHTML
(
 "<html>" .
    "de: {$nome}<br/>" .
    "e-mail:{$email}<br/>" .
    "mensagem:{$mensagem}<br/>" .
"</html>"
);
$mail->AltBody = "de: {$nome}\n" .
                 "e-mail:{$email}\n" .
                 "mensagem:{$mensagem}\n";
if ($mail->send())
{
    colocaMsgSuccess("Mensagem enviado com sucesso.");
    header("Location: index.php");
}
else
{
    colocaMsgDanger("Erro ao enviar mensagem " . $mail->ErrorInfo);
    header("Location: contato.php");
}
die();