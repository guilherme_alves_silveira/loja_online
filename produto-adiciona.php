<?php
require_once("cabecalho.php");
verificaUsuario();
$produto = ProdutoFactory::criaPor($_POST["tipo_produto"]);
$produto->atualizaBaseadoEm($_POST);
$produtoDAO = new ProdutoDAO($conexao);

$isAddSucesso = $produtoDAO->insere($produto);
if ($isAddSucesso)
{
    echo "<p class='text-success'>O produto {$produto->getNome()} de preço {$produto->getPreco()} foi adicionado com sucesso!</p>";
}
else
{
    echo "<p class='text-danger'>O produto {$produto->getNome()} não foi adicionado.</p>";
}

require_once("rodape.php");