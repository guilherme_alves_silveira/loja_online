<?php 
require_once("cabecalho.php"); 
verificaUsuario();
$produtoDAO = new ProdutoDAO($conexao);
$produtos = $produtoDAO->lista();
?>
<h1>Lista de Produtos</h1>
<table class="table table-striped table-bordered">
    <thead>
        <tr class="well">
            <td>Código</td>
            <td>Nome</td>
            <td>Preço</td>
            <td>Imposto</td>
            <td>Descrição</td>
            <td>ISBN</td>
            <td>Categoria</td>
            <td colspan="2">Ações</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($produtos as $produto) : ?>
        <tr>
            <td><?php echo $produto->getId(); ?></td>
            <td><?php echo $produto->getNome(); ?></td>
            <td><?php echo $produto->getPreco(); ?></td>
            <td><?php echo $produto->calculaImposto(); ?></td>
            <td><?php echo $produto->getDescricaoFormatada(); ?></td>
            <td><?php echo $produto->get("Isbn"); ?></td>
            <td><?php echo $produto->getCategoria(); ?></td>
            <td>
                <a class="btn btn-warning btn-sm btn-block" 
                   href="produto-altera-formulario.php?id=<?php echo $produto->getId(); ?>">
                    alterar
                </a>
            </td>
            <form action="produto-remove.php" method="post">
                <input type="hidden" name="id" value="<?php echo $produto->getId(); ?>"/>
                <td><button class="btn btn-danger btn-sm btn-block">remover</button></td>
            </form>
        </tr>
        <?php endforeach; ?>
        </tbody>
</table>
<?php require_once("rodape.php");