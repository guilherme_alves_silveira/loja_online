<?php

session_start();

function usuarioEstaLogado()
{
    return isset($_SESSION["usuario_logado"]);
}

function usuarioLogado()
{
    return $_SESSION["usuario_logado"];
}

function verificaUsuario()
{
    if (!usuarioEstaLogado())
    {
        colocaMsgDanger("Você não tem acesso a essas funcionalidades.");
        header("Location:index.php");
        die();
    }
}

function colocaMsgSuccess($msg)
{
    $_SESSION["success"] = $msg;
}

function colocaMsgDanger($msg)
{
    $_SESSION["danger"] = $msg;
}

function logaUsuario($email)
{
    $_SESSION["usuario_logado"] = $email;
}

function deslogaUsuario()
{
    session_destroy();
    session_start();
}