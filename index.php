<?php require_once("cabecalho.php"); ?>
<h1>Bem vindo!</h1>
<?php

if (usuarioEstaLogado())
{
    ?>
    <p class='alert-success'>
        Usuário <?php echo usuarioLogado(); ?> logado com sucesso.
        <a href='logout.php'>
            <label>Deslogar</label>
            <img src='img/logout.png' alt='LOGOUT' class='img-circle'>
        </a>
    </p>
    <?php
}
else
{
    require_once("formulario-login.php");
}

require_once("rodape.php");