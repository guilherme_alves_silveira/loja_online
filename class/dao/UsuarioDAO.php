<?php
class UsuarioDAO
{
    private $conexao;
    
    public function __construct($conexao)
    {
        $this->conexao = $conexao;
    }
    
    public function busca($email, $senha)
    {
        $senhaTratada = mysqli_real_escape_string($this->conexao, $senha);
        $emailTratado = mysqli_real_escape_string($this->conexao, $email);
        $senhaMd5 = md5($senhaTratada);

        $query = "SELECT id, email FROM usuarios " .
                 "WHERE email = '{$emailTratado}' AND senha = '{$senhaMd5}';";
        $resultado = mysqli_query($this->conexao, $query);
        return mysqli_fetch_assoc($resultado);
    }
}