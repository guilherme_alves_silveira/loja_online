<?php
class ProdutoDAO
{
    private $conexao;
    
    public function __construct($conexao)
    {
        $this->conexao = $conexao;
    }
    
    public function lista()
    {
        $produtos = array();
        $query = "SELECT p.*, c.id As CategoriaID, c.nome As CategoriaNome FROM produtos p "
                . "JOIN categorias c ON c.id = p.categoria_id;";
        $resultado = mysqli_query($this->conexao, $query);
        while ($produtoAtual = mysqli_fetch_assoc($resultado))
        {
            $produto = ProdutoFactory::criaPor($produtoAtual["tipo_produto"]);
            $categoria = new Categoria();
            $categoria->setId($produtoAtual['CategoriaID']);
            $categoria->setNome($produtoAtual['CategoriaNome']);
            $produto->setId($produtoAtual['id']);
            $produto->atualizaBaseadoEm($produtoAtual, $categoria);
            array_push($produtos, $produto);
        }

        return $produtos;
    }

    public function insere(Produto $produto)
    {
        $nomeTratado = mysqli_real_escape_string($this->conexao, $produto->getNome());
        $precoTratado = mysqli_real_escape_string($this->conexao, $produto->getPreco());
        $descricaoTratado = mysqli_real_escape_string($this->conexao, $produto->getDescricao());
        $categoriaIdTratado = mysqli_real_escape_string($this->conexao, $produto->getCategoriaId());
        $usadoTratado = mysqli_real_escape_string($this->conexao, $produto->getUsado());
        $tipoTratado = mysqli_real_escape_string($this->conexao, $produto->get("TipoProduto"));
        $isbnTratado = mysqli_real_escape_string($this->conexao, $produto->get("Isbn"));
        $waterMarkTratado = mysqli_real_escape_string($this->conexao, $produto->get("WaterMark"));
        $taxaTratado = mysqli_real_escape_string($this->conexao, $produto->get("TaxaImpressao"));
        //********************************************************
        $isbnTratado = $this->getNULL($isbnTratado, true);
        $tipoTratado = $this->getNULL($tipoTratado, true);
        $taxaTratado = $this->getNULL($taxaTratado, true);
        $waterMarkTratado = $this->getNULL($waterMarkTratado);
        //********************************************************
        $query = "INSERT INTO produtos
            (
                nome,
                preco, 
                descricao,
                categoria_id, 
                usado,
                isbn,
                tipo_produto,
                water_mark,
                taxa_impressao
            ) 
            VALUES
            (
                '{$nomeTratado}',
                {$precoTratado},
                '{$descricaoTratado}',
                {$categoriaIdTratado}, 
                {$usadoTratado},
                {$isbnTratado},
                {$tipoTratado},
                {$waterMarkTratado},
                {$taxaTratado}
            );";
                
        return mysqli_query($this->conexao, $query);
    }

    public function altera(Produto $produto)
    {
        $idTratado = mysqli_real_escape_string($this->conexao, $produto->getId());
        $nomeTratado = mysqli_real_escape_string($this->conexao, $produto->getNome());
        $precoTratado = mysqli_real_escape_string($this->conexao, $produto->getPreco());
        $descricaoTratado = mysqli_real_escape_string($this->conexao, $produto->getDescricao());
        $categoriaIdTratado = mysqli_real_escape_string($this->conexao, $produto->getCategoriaId());
        $usadoTratado = mysqli_real_escape_string($this->conexao, $produto->getUsado());
        $isbnTratado = mysqli_real_escape_string($this->conexao, $produto->get("Isbn"));
        $tipoTratado = mysqli_real_escape_string($this->conexao, $produto->get("TipoProduto"));
        $waterMarkTratado = mysqli_real_escape_string($this->conexao, $produto->get("WaterMark"));
        $taxaTratado = mysqli_real_escape_string($this->conexao, $produto->get("TaxaImpressao"));
        //********************************************************
        $isbnTratado = $this->getNULL($isbnTratado, true);
        $tipoTratado = $this->getNULL($tipoTratado, true);
        $taxaTratado = $this->getNULL($taxaTratado, true);
        $waterMarkTratado = $this->getNULL($waterMarkTratado);
        //********************************************************
        $query = "UPDATE produtos 
                  SET 
                    nome = '{$nomeTratado}', 
                    preco = {$precoTratado}, 
                    descricao = '{$descricaoTratado}', 
                    categoria_id = {$categoriaIdTratado}, 
                    usado = {$usadoTratado},
                    isbn = '{$isbnTratado}',
                    tipo_produto = {$tipoTratado},
                    water_mark = {$waterMarkTratado},
                    taxa_impressao = {$taxaTratado}
                  WHERE id = {$idTratado}";
        return mysqli_query($this->conexao, $query);
    }

    public function remove($id)
    {
        $idTratado = mysqli_real_escape_string($this->conexao, $id);
        $query = "DELETE FROM produtos WHERE id = {$idTratado}";
        return mysqli_query($this->conexao, $query);
    }

    public function buscaPorId($id)
    {
        $idTratado = mysqli_real_escape_string($this->conexao, $id);
        $query = "SELECT * FROM produtos WHERE id = {$idTratado}";
        $resultado = mysqli_query($this->conexao, $query);
        $produtoAtual = mysqli_fetch_assoc($resultado);
        $produto = ProdutoFactory::criaPor($produtoAtual['tipo_produto']);
        $produto->setId($produtoAtual['id']);
        $produto->atualizaBaseadoEm($produtoAtual);
        return $produto;
    }
    
    private function getNULL($vazio, $colocaAspas = false)
    {
        if($vazio === null || $vazio === "")
        {
            return "NULL";
        }
        else if ($colocaAspas)
        {
            return "'" . $vazio . "'";
        }
        
        return $vazio;
    }
}