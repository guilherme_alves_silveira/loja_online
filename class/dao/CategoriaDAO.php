<?php
class CategoriaDAO
{
    private $conexao;
    
    public function __construct($conexao)
    {
        $this->conexao = $conexao;
    }
    
    public function lista()
    {
        $categorias = array();
        $query = "SELECT * FROM categorias";
        $resultado = mysqli_query($this->conexao, $query);

        while ($categoriaAtual= mysqli_fetch_assoc($resultado))
        {
            $categoria = new Categoria();
            $categoria->setId($categoriaAtual['id']);
            $categoria->setNome($categoriaAtual['nome']);
            array_push($categorias, $categoria);
        }

        return $categorias;
    }
}