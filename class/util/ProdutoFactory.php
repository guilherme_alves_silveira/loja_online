<?php
class ProdutoFactory
{
    private static $classesPermitidas = array("LivroFisico", "Ebook");
    
    public static function criaPor($tipoProduto)
    {
        if (in_array($tipoProduto, self::$classesPermitidas))
        {
            $novoTipoProduto = new $tipoProduto;
            $novoTipoProduto->setTipoProduto($tipoProduto);
            return $novoTipoProduto;
        }
        else
        {
            throw new InvalidArgumentException("Tipo de produto inexistente!");
        }
    }
    
    public function __invoke($tipoProduto)
    {
        return self::criaPor($tipoProduto);
    }
}