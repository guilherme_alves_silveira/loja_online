<?php
abstract class Livro extends Produto
{
    private $isbn;
    
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;
    }
    
    public function getIsbn()
    {
        return $this->isbn;
    }
    
    public function calculaImposto()
    {
        return $this->getPreco() * 0.065;
    }
    
    public function atualizaBaseadoEm(array $params, Categoria $categoria = null)
    {
        $this->setNome($params["nome"]);
        $this->setPreco($params["preco"]);
        $this->setDescricao($params["descricao"]);
        if($categoria === null)
        {
            $this->setCategoriaId($params["categoria_id"]);
        }
        else
        {
            $this->setCategoria($categoria);
        }
        $ehUsado = array_key_exists("usado", $params)? "1" : "0";
        $this->setUsado($ehUsado);
        $this->setIsbn($params['isbn']);
    }
}