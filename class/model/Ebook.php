<?php
class Ebook extends Livro
{
    private $waterMark;
    
    public function getWaterMark()
    {
        return $this->waterMark;
    }
    
    public function setWaterMark($waterMark)
    {
        $this->waterMark = $waterMark;
    }
    
    public function atualizaBaseadoEm(array $params, Categoria $categoria = null)
    {
        parent::atualizaBaseadoEm($params, $categoria);
        $this->setWaterMark($params["water_mark"]);
    }
}