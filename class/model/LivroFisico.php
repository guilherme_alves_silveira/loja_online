<?php
class LivroFisico extends Livro
{
    private $taxaImpressao;
    
    public function getTaxaImpressao()
    {
        return $this->taxaImpressao;
    }
    
    public function setTaxaImpressao($taxaImpressao)
    {
        $this->taxaImpressao = $taxaImpressao;
    }
    
    public function atualizaBaseadoEm(array $params, Categoria $categoria = null)
    {
        parent::atualizaBaseadoEm($params, $categoria);
        $this->setTaxaImpressao($params["taxa_impressao"]);
    }
}