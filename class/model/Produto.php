<?php

abstract class Produto
{
    private $id;
    private $nome;
    private $preco;
    private $descricao;
    private $categoria;
    private $usado;
    private $tipoProduto;

    public function __construct($nome = "Produto não definido", $preco = 99999)
    {
        $this->setNome($nome);
        $this->setPreco($preco);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    public function getPreco()
    {
        return $this->preco;
    }

    public function setPreco($preco)
    {
        $this->preco = $preco;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function setCategoria(Categoria $categoria)
    {
        if ($categoria === null)
        {
            throw new InvalidArgumentException("A categoria passada não pode ser nula.");
        }

        $this->categoria = $categoria;
    }

    public function getUsado()
    {
        return $this->usado;
    }

    public function setUsado($usado)
    {
        $this->usado = $usado;
    }
    
    public function getTipoProduto()
    {
        return $this->tipoProduto;
    }
    
    public function setTipoProduto($tipoProduto)
    {
        $this->tipoProduto = $tipoProduto;
    }
    
    public function getCategoriaId()
    {
        if ($this->categoria === null)
        {
            return 0;
        }

        return $this->categoria->getId();
    }

    public function setCategoriaId($id)
    {
        if ($this->categoria === null)
        {
            $this->categoria = new Categoria();
        }

        $this->categoria->setId($id);
    }

    public function getDescricaoFormatada()
    {
        return substr($this->descricao, 0, 40);
    }

    public function subtraiPreco($valor = 0.05)
    {
        if ($valor < 0 || $valor > 1)
        {
            throw new InvalidArgumentException("O desconto deve ir somente de 0 até 1.");
        }

        $this->preco -= $this->preco * $valor;
        return $this->preco;
    }

    public function calculaImposto()
    {
        return $this->preco * 0.195;
    }
    
    public function __toString()
    {
        return $this->nome;
    }
     
    public function get($funcaoChamada)
    {   
        $funcaoChamada = "get" . $funcaoChamada;
        if ($this->ehAMesmaFuncao($funcaoChamada, "getIsbn"))
        {
            return $this->getIsbn();
        }
        else if ($this->ehAMesmaFuncao($funcaoChamada, "getWaterMark"))
        {
            return $this->getWaterMark();
        }
        else if ($this->ehAMesmaFuncao($funcaoChamada, "getTaxaImpressao"))
        {
            return $this->getTaxaImpressao();
        }
        
        return null;
    }
    
    private function ehAMesmaFuncao($funcaoChamada, $funcaoExistente)
    {
        return method_exists($this, $funcaoChamada) && $funcaoChamada === $funcaoExistente;
    }
    
    public abstract function atualizaBaseadoEm(array $params, Categoria $categoria = null);
}