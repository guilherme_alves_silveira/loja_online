<?php 
require_once("cabecalho.php");
verificaUsuario();
$categoriaDAO = new CategoriaDAO($conexao);
$categorias = $categoriaDAO->lista();

$usado = "";
$produto = new Produto();
$produto->setNome("");
$produto->setPreco("");
$produto->setDescricao("");
$produto->setCategoriaId("1");
?>
<h1>Adicione o Produto</h1>
<form action="produto-adiciona.php" method="post">
    <table class="table">
        <?php require_once("produto-formulario-base.php"); ?>
        <tr>
            <td><input class="btn btn-primary btn-lg" type="submit" value="Adicionar Produto"/></td>
        </tr>
    </table>
</form>
<?php require_once("rodape.php");