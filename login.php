<?php
require_once("util/autoload.php");
require_once("conecta.php");
require_once("autenticacao.php");
$usuarioDAO = new UsuarioDAO($conexao);
$usuario = $usuarioDAO->busca($_POST["email"], $_POST["senha"]);

if ($usuario == null)
{
    colocaMsgDanger("Usuário ou senha inválidos.");
}
else
{
    logaUsuario($_POST["email"]);
}
header("Location:index.php");
die();