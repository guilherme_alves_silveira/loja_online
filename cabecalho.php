<?php 
error_reporting(E_ALL ^ E_NOTICE);
require_once("util/autoload.php");
require_once("conecta.php");
require_once("autenticacao.php");
require_once("mostra-alerta.php"); 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Minha loja</title>
        <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.css" rel="stylesheet" />
        <link href="css/loja.css" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!-- Menu -->
        <div id='cssmenu'>
            <ul>
               <li class='active'><a href='index.php'><span>Minha Loja</span></a></li>
               <li class='has-sub'><a><span>Produto</span></a>
                    <ul>
                        <li class='has-sub'><a href='produto-adiciona-formulario.php'>
                            <span>Adiciona Produto</span></a>
                        </li>
                        <li class='has-sub'><a href='produto-lista.php'>
                            <span>Lista Produto</span></a>
                        </li>
                    </ul>
               </li>
               <li><a href='contato.php'><span>Contato</span></a></li>
               <li><a href='sobre.php'><span>Sobre</span></a></li>
            </ul>
        </div>
        <!-- Conteúdo -->
        <div class="container">
                <div class="principal">
                <br><br>
                <?php 
                    mostraAlerta("success"); 
                    mostraAlerta("danger");