<?php
require_once("util/autoload.php");
require_once("conecta.php");
require_once("autenticacao.php");

verificaUsuario();
$id = $_POST["id"];
$produtoDAO = new ProdutoDAO($conexao);
$resultado = $produtoDAO->remove($id);

if ($resultado)
{
    colocaMsgSuccess("Produto removido com sucesso.");
    header("Location:produto-lista.php");
}
else
{
    colocaMsgDanger("Não foi possível remover o produto.");
    header("Location:produto-lista.php");
}
die();
